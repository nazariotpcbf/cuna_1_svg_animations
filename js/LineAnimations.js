
"use strict";

class LineAnimations {
	constructor(pTarget) {
		
		this.binds();
		this.target = pTarget;
		this.path = this.target.getElementsByClassName("center_line")[0];
		this.circleStart = new SvgCircle(this.target.getElementsByClassName("edge_circ_left")[0]);
		this.circle1 = new SvgCircle(this.target.getElementsByClassName("idea_circ_1")[0],this.target.getElementsByClassName("light_bulb")[0],55,69);
		this.circle2 = new SvgCircle(this.target.getElementsByClassName("idea_circ_2")[0],this.target.getElementsByClassName("falling_numbers")[0],54,64);
		this.circle3 = new SvgCircle(this.target.getElementsByClassName("idea_circ_3")[0],this.target.getElementsByClassName("shopping_cart")[0],75,65);
		this.circleEnd = new SvgCircle(this.target.getElementsByClassName("edge_circ_right")[0]);

		let t1 = new TweenLite(this.circleStart,4,{delay:.1,ease:Power3.easeInOut,x:10});
		let t2 = new TweenLite(this.circleEnd,4,{delay:.1,ease:Power3.easeInOut,x:742});

		let startTime1=1.5;
		let startTime3=1.75;
		let startTime2=2.6;
		//--
		let slideDuration=2;
		let slideEase = Power4.easeInOut;

		TweenLite.set(this.circleStart.target,{ drawSVG:"0%"});
		TweenLite.set(this.circleEnd.target,{ drawSVG:"0%"});


		let t3 = new TweenLite(this.circle1,slideDuration,{ease:slideEase,delay:startTime1,x:"-=220"});
		let t4 = new TweenLite(this.circle1,3.3,{ease:Expo.easeOut,delay:startTime1-.5,radius:76});
		let t5 = this.circle1.animateIn(startTime1+.7);


		let t6 = new TweenLite(this.circle2,slideDuration,{ease:slideEase,delay:startTime2});
		let t7 = new TweenLite(this.circle2,3.3,{ease:Expo.easeOut,delay:startTime2-.5,radius:76});
		let t8 = this.circle2.animateIn(startTime2+.3);

		let t9 = new TweenLite(this.circle3,slideDuration,{ease:slideEase,delay:startTime3,x:"+=220"});
		let t10 = new TweenLite(this.circle3,3.3,{ease:Expo.easeOut,delay:startTime3-.5,radius:76});
		let t11 = this.circle3.animateIn(startTime3+.7);

		let t12 = new TweenLite(this.circleEnd.target,1,{ease:Expo.easeInOut,drawSVG:"100%"});
		let t13 = new TweenLite(this.circleStart.target,1,{ ease:Expo.easeInOut,drawSVG:"100%"});


		this.timeline  =new TimelineLite({tweens:[t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13],onUpdate:this.updatePath,paused:true});
		this.timeline.timeScale(2);
		this.updatePath();


	};

	animateIn (pDelay) {
		pDelay = (!pDelay)?0:pDelay;
		TweenLite.delayedCall(pDelay, () =>{ this.timeline.resume()});
	}

	binds() {
		this.updatePath = this.updatePath.bind(this);
	};

	translateInBetween(pCircle) {
		let response = "";
		let sideX = pCircle.sideX;
		response += "L " +sideX+ " " + pCircle.y;
		response += " M " +(sideX+pCircle.diameter)+ " " + pCircle.y;
		//
		return response;
	}
;


	translateStartPos(pCircle) {
		let response = "";
		response += "M " +pCircle.sideXRight+ " " + pCircle.y;
		return response;
	};

	translateEndPos(pCircle) {
		let response = "";
		response += "L " +pCircle.sideX+ " " + pCircle.y;
		return response;
	};


	updatePath() {
		var diffX= this.circleEnd.x - this.circleStart.x;
		let path = "";
		if (diffX<3)return;
		if (this.circleStart.x == this.circleEnd.x)return;
		path += this.translateStartPos(this.circleStart);
		if (this.circle1.radius >0)path += this.translateInBetween(this.circle1);
		if (this.circle2.radius >0)path += this.translateInBetween(this.circle2);
		if (this.circle3.radius >0)path += this.translateInBetween(this.circle3);
		path += " "+this.translateEndPos(this.circleEnd,true)+"z";
		this.path.setAttribute("d", path);
	}
;

}