/**
 * Created by NAZ on 20/10/15.
 */

"use strict";
class SvgCircle {
	constructor(pTarget,pTargetSymbol,pTargetW,pTargetH,pColor) {
		this.updateTargetScale = this.updateTargetScale.bind(this);
		this.target = pTarget;
		//
		this.scaleDummy = {val:.5};
		this.targetSymbol = pTargetSymbol;
		this.targetW =pTargetW*.5;
		this.targetH =pTargetH*.5;
		this.x = ~~(this.target.getAttribute("cx"));
		this.y = ~~(this.target.getAttribute("cy"));
		this.radius = ~~(this.target.getAttribute("r"));
		this.stroke = ~~(this.target.getAttribute("stroke-width"));
		this.strokeHalf = (this.stroke * .5);
		this.rgb =(!pColor)? {r:255,g:255,b:255} :pColor ;

		if (this.targetSymbol){
			let children = this.targetSymbol.getElementsByTagName('*');

			let i;
			let classname;
			for(i = 0; i < children.length; i++) {
				classname = children[i].getAttribute('class');

				if(classname) {
					if(classname.indexOf('fill') > -1) {
						if(!this.targetFill) {
							this.targetFill = children[i];
						}
						else {
							continue;
						}
					}

					if(classname.indexOf('border') > -1) {
						if(!this.targetBorder) {
							this.targetBorder = children[i];
						}
						else {
							continue;
						}
					}
				}

			}

			this.colorAnimF =  new Color(this.rgb.r,this.rgb.g,this.rgb.b,0);
			this.colorAnimF.setTarget(this.targetFill,"fill");

			this.colorAnimB =  new Color(this.rgb.r,this.rgb.g,this.rgb.b,1);
			this.colorAnimB.setTarget(this.targetBorder,"stroke");

			let paths = this.targetBorder.getElementsByTagName("path");
			if (paths && paths.length>0){
				this.borderPaths = paths;
				TweenLite.set(this.borderPaths,{ drawSVG:"0%"});
			}else TweenLite.set(this.targetBorder,{ drawSVG:"0%"});
		}
	};


	updateTargetScale() {
		let offScale = this.scaleDummy.val;
		let offsetX = (offScale) * (this.targetW);
		let offsetY = (offScale) * (this.targetH);
		if (this.xVal && this.yVal)this.targetSymbol.setAttribute("transform","matrix("+this.scaleDummy.val+","+0+","+0+","+this.scaleDummy.val+","+(this.x - offsetX)+","+(this.y - offsetY)+")");
	}

	animateIn(pDelay){
		pDelay = (!pDelay)?0:pDelay;
		let t3 = null;
		var crossFadeTime = 1.5;
		let t1 = new TweenLite(this.scaleDummy,1,{delay:.1,val:1,ease:Power2.easeOut,onUpdate:this.updateTargetScale});
		let t2 = this.colorAnimF.animateToColor(this.rgb.r,this.rgb.g,this.rgb.b,1,.8,crossFadeTime);
		//--
		if (this.borderPaths) t3 = new TweenLite(this.borderPaths,2.5,{ease:Linear.easeNone, drawSVG:"100%"});
		else  t3 = new TweenLite(this.targetBorder,2.5,{ease:Linear.easeNone, drawSVG:"100%"});
		//--
		let t4 = this.colorAnimB.animateToColor(this.rgb.r,this.rgb.g,this.rgb.b,0,1,crossFadeTime);
		return new TimelineLite({tweens:[t1,t2,t3,t4],delay:pDelay});

	}
	update() {
		if (this.xVal)this.target.setAttribute("cx", this.xVal);
		if (this.yVal)this.target.setAttribute("cy", this.yVal);
		if (this.radiusVal)this.target.setAttribute("r", this.radiusVal);
		if (this.strokeVal)this.target.setAttribute("stroke-width", this.strokeVal);
	}

	positionTargetSymbol()	{
		this.updateTargetScale();
	}

	//------------------------------------------

	get sideX() {
		return this.xVal - (this.radiusVal + this.strokeHalf );
	}

	get sideXRight() {
		return this.xVal + (this.radiusVal + this.strokeHalf );
	}

	get sideY() {
		return this.yVal - (this.radiusVal + this.strokeHalf );
	}

	get sideYTop() {
		return this.yVal + (this.radiusVal + this.strokeHalf );
	}



	get width() {
		let response = this.diameter - this.strokeHalf;
		if (response < 0)return 0;
		else return response;
	}
	//------------------------------------------

	get x() {
		return this.xVal;
	}

	set x(pVal) {
		this.xVal = pVal;
		this.update();
		if (this.targetSymbol)this.positionTargetSymbol();
	}

	//------------------------------------------

	get y() {
		return this.yVal;
	}

	set y(pVal) {
		this.yVal = pVal;
		this.update();
		if (this.targetSymbol)this.positionTargetSymbol();
	}

	//------------------------------------------

	get radius() {
		return this.radiusVal;
	}

	set radius(pVal) {
		this.radiusVal = pVal;
		this.diameter = (this.radiusVal*2);
		this.update();
	}

	//------------------------------------------

	get stroke() {
		return this.strokeVal;
	}

	set stroke(pVal) {
		this.strokeVal = pVal;
		this.update();

	}

}
